var mymap = L.map('mapid').setView([-6.878059, -79.917046], 18);//que ubique el mapa en esa coordenada
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
/*
attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
*/
    attribution: '&copy; <a href="https://www.osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);


L.marker([-6.878059, -79.917046]).addTo(mymap)
    .bindPopup('Ubícanos cerca a la Bombonera.<br> Estamos las 24 horas.').addTo(mymap)
    //.openPopup();

L.marker([-6.877565, -79.919793]).addTo(mymap)
    .bindPopup('Ubícanos frente a la I.E José Olaya Balandra.<br> Estamos las 24 horas.').addTo(mymap)
    //.openPopup();

L.marker([-6.883738, -79.921665]).addTo(mymap)
    .bindPopup('Ubícanos cerca a la Playa <br> Estamos las 24 horas.').addTo(mymap)
    //.openPopup();





$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(mymap);

        });
    }

})