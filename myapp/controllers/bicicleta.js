var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis(function(err,bicis){
        res.render('bicicletas/index',{bicis:bicis});
    });
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res){
    /*var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
    bici.ubicacion = [req.body.lat, req.body.lng];*/
    var ub = [];
    ub=[req.body.lat,req.body.lng];
    var bici = Bicicleta.createInstance(req.body.code,req.body.color,req.body.modelo,ub);
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res){
    /*var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update',{bici});*/
    Bicicleta.findById(req.params.id,function(err,abici){
        res.render('bicicletas/update',{bici:abici});
    })
}

exports.bicicleta_update_post = function (req, res){
    /*var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];*/

    Bicicleta.updateById(req.params.id,req.body.code,req.body.color,req.body.modelo,req.body.lat,req.body.lng,function (err,succ){
        res.redirect('/bicicletas');
    })

    //res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req, res){
    /*Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');*/
    Bicicleta.removeById(req.params.id, function(cb){
        res.redirect('/bicicletas');
    });
}