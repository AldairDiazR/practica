var Bicicleta = require ('../../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({
            bicicletas : bicis
        });
    }) 
}

exports.bicicleta_create = function (req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color,req.body.modelo);
    bici.ubicacion=[req.body.lat, req.body.lng];
    console.log(bici);
    Bicicleta.add(bici, function(err,aBici){
        res.status(200).json({
            bicicleta : aBici
        })
    });
}

exports.bicicleta_update= function(req,res){
    Bicicleta.updateByCode(req.params.code,req.body.code,req.body.color,req.body.modelo,req.body.lat,req.body.lng,function(error,doc){
        if(error){
            res.status(500).send();
        }else{
            res.status(200).send();
        }
    });
}

exports.bicicleta_delete =function(req, res){
    Bicicleta.removeByCode(req.params.code, function(err, bici){
        res.status(204).send();
    }) 
}
